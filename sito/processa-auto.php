<?php
require_once "bootstrap.php";
require_once "notifiche.php";

$templateParams["allUsersNotification"]=$dbh->sendAllUserNotification();

if(!isUserLoggedIn() || !isset($_POST["action"])){
  require("template/base.php");
}

    $model = $_POST["modello"];
    $kms = $_POST["kms"];
    $brand = $_POST["marca"];
    $color = $_POST["colore"];
    $fuelType = $_POST["alimentazione"];
    $transmission = $_POST["cambio"];
    $engine = $_POST["motore"];
    $price = $_POST["prezzo"];
    $productionYear = $_POST["annodiproduzione"];
    $carCondition = $_POST["condizione"];
    $carDescription = $_POST["descrizione"];

    if(isset($_FILES["image"]) && strlen($_FILES["image"]["name"])>0) {
        list($result, $msg) = uploadImage(UPLOAD_DIR, $_FILES["image"]);
        if ($result!=0) {
            $imgcar = $msg;
        }
    }

    if($_POST["action"]==1){
        //Inserisco la macchina
        $isSold = "false";
            if(!isset($result) || $result == 0) {
                $imgcar="peroraniente";
            }
            $id = $dbh->insertCar($model, $color, $kms, $transmission, $engine, $fuelType, $brand);
            $text = getNotificationTextType(2);
            foreach($templateParams["allUsersNotification"] as $allusers) {
                $dbh->createNotification($allusers["e_mail"], $text, 2);
            }
            if($id != 0){
                $dbh->insertCarSaleInfo($id, $price, $carCondition, $productionYear, $carDescription, $isSold, $imgcar);
                $_SESSION["msg"] = "Inserimento completato correttamente!";
            }
            else{
                $_SESSION["msg"] = "Errore nell'inserimento!";
            }
        } elseif($_POST["action"]==2){
        //modifico la macchina
        $issold = $_POST["issold"];
        $ordine = $_POST["orderid"];
        $oldcarID = $_POST["carID"];
        $newcarID = $_POST["id"];

        if(!isset($result) || $result==0) {
            $imgcar = $_POST["oldimage"];
        }
        $dbh->updateCar($oldcarID, $model, $color, $kms, $transmission, $engine, $fuelType, $ordine, $newcarID, $brand);
        $dbh->updateCarSaleInfo($oldcarID, $price, $carCondition, $productionYear, $carDescription, $issold, $imgcar, $newcarID);
        $text = getNotificationTextType(5);
        foreach($templateParams["allUsersNotification"] as $allusers){
            $dbh->createNotification($allusers["e_mail"], $text, 5);
        }
        $_SESSION["msg"] = "Modifica completata correttamente!";

        } elseif($_POST["action"]==3){
            //cancello la macchina
            $carID = $_POST["carID"];
            $dbh->deleteCarSaleInfo($carID);
            $dbh->deleteCar($carID);
            $text = getNotificationTextType(8);
            foreach($templateParams["allUsersNotification"] as $allusers){
                $dbh->createNotification($allusers["e_mail"], $text, 8);
            }
            $_SESSION["msg"] = "Cancellazione completata correttamente!";
        }

    header("location: profilo.php");


?>
