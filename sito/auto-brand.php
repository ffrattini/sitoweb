<?php
require_once "bootstrap.php";

//Base Template
$loginTemplate["titolo"] = "Garagem";
$loginTemplate["main"] = "login-form.php";
$templateParams["brand"] = $dbh->getBrand();

$brandID = -1;
if(isset($_GET["id"])){
    $brandID = $_GET["id"];
}
$nomebrand = $dbh->getBrandById($brandID);
if(count($nomebrand)>0){
    $templateParams["titolo_pagina"] = "Brand auto".$nomebrand[0]["brandName"];
    $templateParams["macchine"] = $dbh->getCarByBrand($idcategoria);
}
else{
    $templateParams["titolo_pagina"] = "Brand non trovato";
    $templateParams["macchine"] = array();
}

require("template/base.php");
?>
