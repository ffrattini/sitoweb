<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Garagem";
$templateParams["main"] = "template/foto.php";
$templateParams["img"] = "homepage.jpg";
$templateParams["img-recensione"] = "Sfondo_recensione_cliente.jpg";
$templateParams["autorandom"] = $dbh->getRandomCars(3);
$templateParams["reviews"] = $dbh->GetReviews();

require 'template/base.php';
?>
