<main class="noaside">
    <section style="margin-top:20px";>
      <h2 style="text-align:left; ">La mia pagina personale</h2>
        <button class="btn" id="accountdetails"><i class="fa fa-user"> Dettagli Account</i></button>
        <div id="menu1" style="display:none;" >
            <?php $user = $templateParams["userinfo"][0]; ?>
              <p style="font-weight:bold;">Nome: <label style="font-weight:initial;"> <?php echo $user["name"]; ?></label></p>
              <p style="font-weight:bold;">E-Mail: <label style="font-weight:initial;"><?php echo $user["e_mail"]; ?></label></p>
        </div>

        <button class="btn" id="myorders"><i class="fa fa-first-order"> I miei ordini</i></button>
        <div id="menu2" style="display:none;">
            <?php foreach($templateParams["orderinfo"] as $order): ?>
              <p style="font-weight:bold;">ID Ordine: <label style="font-weight:initial;"><?php echo $order["orderID"]; ?></label></p>
              <p style="font-weight:bold;">Data Ordine: <label style="font-weight:initial;"><?php echo $order["orderDate"]; ?></label></p>
              <p style="font-weight:bold;">Marca auto: <label style="font-weight:initial;"><?php echo $order["brandName"]; ?></label></p>
              <p style="font-weight:bold;">Modello: <label style="font-weight:initial;"><?php echo $order["model"]; ?></label></p>
              <p style="font-weight:bold;">Colore: <label style="font-weight:initial;"><?php echo $order["color"]; ?></label></p>
              <p style="font-weight:bold;">Motore: <label style="font-weight:initial;"><?php echo $order["engine"]; ?></label></p>
              <p style="font-weight:bold;">Tipo Cambio: <label style="font-weight:initial;"><?php echo $order["transmission"]; ?></label></p>
              <p style="font-weight:bold;">Alimentazione: <label style="font-weight:initial;"><?php echo $order["fuelType"]; ?></label></p>
              <p style="font-weight:bold;">Prezzo: <label style="font-weight:initial;"><?php echo $order["price"]; ?> €</label></p>
              <p style="font-weight:bold;">Descrizione auto: <label style="font-weight:initial;"><?php echo 'stato auto: '.$order["carCondition"].'<br>'.$order["carDescription"]; ?></label></p>
            <?php endforeach; ?>
          <?php if($templateParams["orderinfo"] == null) : ?>
            <p> Nessun ordine è stato effettuato </p>
          <?php endif; ?>

        </div>
          <button class="btn" id="notification"><i class="fa fa-bell"> Notifiche</i></button>
          <div id="menu3" style="display:none;">
            <?php foreach($templateParams["notifiche"] as $notification): ?>
              <div class="notification">
                <span class="chiudi" id="<?php echo 'close'.$notification["notificationID"]; ?>" data-id="<?php echo $notification["notificationID"]; ?>">&times;</span>
                <strong><?php echo $notification["notificationDate"];?></strong><br>
                <?php echo $notification["notificationText"];?>
              </div>
            <?php endforeach; ?>
          <?php if($templateParams["notifiche"] == null) : ?>
            <p> Nessuna notifica da visualizzare </p>
          <?php endif; ?></div>
        </div>
    </section>

    <section>
      <h2 style="text-align:left; "> Area amministrazione del sito </h2>
      <?php if(isset($_SESSION["msg"])): ?>
        <p style="text-align:center;"><?php echo $_SESSION["msg"]; ?></p>
        <?php unset($_SESSION["msg"]); ?>
      <?php endif; ?>

      <a href="gestisci-auto.php?action=1"><button class="btn" id="newcar"><i class="fa fa-plus" aria-hidden="true"> Inserisci nuova auto</i></button></a>
      <button class="btn" id="allorders"><i class="fa fa-first-order"> Visualizza tutti gli ordini</i></button>
      <div id="menu4" style="display:none;">
          <?php foreach($templateParams["allordersinfo"] as $order): ?>
            <p style="font-weight:bold;">ID Ordine - Data Ordine: <label style="font-weight:initial;"><?php echo $order["orderID"]; ?> - <?php echo $order["orderDate"]; ?></label></p>
            <p style="font-weight:bold;">Nome utente - email: <label style="font-weight:initial;"><?php echo $order["name"]; ?> - <?php echo $order["e_mail"]; ?></label></p>
            <p style="font-weight:bold;">Marca auto - Modello: <label style="font-weight:initial;"><?php echo $order["brandName"]; ?> - <?php echo $order["model"]; ?></label></p>
            <p style="font-weight:bold;">Colore: <label style="font-weight:initial;"><?php echo $order["color"]; ?></label></p>
            <p style="font-weight:bold;">Motore - Tipo Cambio - Alimentazione: <label style="font-weight:initial;"><?php echo $order["engine"]; ?> - <?php echo $order["transmission"]; ?> - <?php echo $order["fuelType"]; ?></label></p>
            <p style="font-weight:bold;">Prezzo: <label style="font-weight:initial;"><?php echo $order["price"]; ?> €</label></p>
          <?php endforeach; ?>
          <?php if($templateParams["allordersinfo"] == null) : ?>
            <p> Nessun ordine è stato ancora effettuato sul sito </p>
          <?php endif; ?></div>
      </div>

      <table>
          <tr>
            <th>Dettagli auto</th><th>Descrizione</th><th>Azione</th>
          </tr>
      <?php foreach($templateParams["macchine"] as $car): ?>
          <tr>
            <td headers="dettagli auto">
              <h2> <?php echo $car["carID"]; ?> - <?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?> </h2>
              <p> Colore: <?php echo $car["color"]; ?> </p>
              <p> Km: <?php echo $car["kms"]; ?> </p>
              <p> Alimentazione: <?php echo $car["fuelType"]; ?> </p>
              <p> Cambio: <?php echo $car["transmission"]; ?> </p>
              <p> Motore: <?php echo $car["engine"]; ?> </p>
              <p> Prezzo: <?php echo $car["price"]; ?> </p>
              <p> Anno di produzione: <?php echo $car["productionYear"]; ?> </p>
              <p> Condizione auto: <?php echo $car["carCondition"]; ?> </p>
            </td>
            <td headers="descrizione">
              <img src="<?php echo UPLOAD_DIR.$car["image"]; ?>" class="tabella" width="80%" alt="" />
              <textarea title="descrizione auto" id="descrizione" class="tabella" readonly><?php echo $car["carDescription"]; ?></textarea>
              
            </td>
            <td headers="azioni">
              <a href="gestisci-auto.php?action=2&id=<?php echo $car["carID"]; ?>"><button class="btn" id="prof" title="Modifica Auto"><i class="fa fa-edit" aria-hidden="true" title="Modifica Auto"></i></button><span class="sr-only">Modifica Auto</span></a>
              <a href="gestisci-auto.php?action=3&id=<?php echo $car["carID"]; ?>"><button class="btn" id="prof" title="Cancella Auto"><i class="fa fa-trash" aria-hidden="true"></i></button><span class="sr-only">Elimina Auto</span></a>
            </td>
          </tr>
      <?php endforeach; ?>
    </table>
    </section>

</main>
