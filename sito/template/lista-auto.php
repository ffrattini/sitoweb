        <aside id="pannello">
        <div>
            <h3>Filtra Risultati</h3>
        </div>

        <form id="ricerca" name="ricerca" action="ricerca-auto.php"  method="POST">
            <div class="cntr">
                <div class="cntr-innr">
                    <label class="search" for="search">
                        <input type="text" id="search" name="ricerca" placeholder="Inserisci marca, modello..." autocomplete="off" />
                        <span class="sr-only">Search</span>
                    </label>
                </div>
            </div>
        </form>

        <button class="btn" id="filter" title="filtri di ricerca"><i class="fa fa-filter"></i></button>
        <section id="filtro" style="display:none">
            <form id="filtro" action="filtri-auto.php" method="POST">
                <div id="alimentazione">
                <p>Alimentazione</p>
                    <input type="checkbox" id="benza" name="carburante[]" value="benzina">
                    <label for="benza">Benzina</label><br>
                    <input type="checkbox" id="diesel" name="carburante[]" value="diesel">
                    <label for="diesel">Diesel</label><br>
                    <input type="checkbox" id="gpl" name="carburante[]" value="gpl">
                    <label for="gpl">GPL</label><br>
                    <input type="checkbox" id="metano" name="carburante[]" value="metano">
                    <label for="metano">Metano</label>
                </div>
                <div class="slidecontainer">
                    <p>Anno a partire da <span id="demo"></span></p>
                    <input type="range" id="anno" class="slider" min="1990" max="2020" value="2020" >
                </div>
                <div id="marca">
                    <p>Marca</p>
                    <input type="checkbox" id="marca1" name="marca[]" value="abarth">
                    <label for="marca1">Abarth</label><br>
                    <input type="checkbox" id="marca2" name="marca[]" value="alfa romeo">
                    <label for="marca2">Alfa Romeo</label><br>
                    <input type="checkbox" id="marca3" name="marca[]" value="audi">
                    <label for="marca3">Audi</label><br>
                    <input type="checkbox" id="marca4" name="marca[]" value="bmw">
                    <label for="marca4">BMW</label><br>
                    <input type="checkbox" id="marca5" name="marca[]" value="citroen">
                    <label for="marca5">Citroen</label><br>
                    <input type="checkbox" id="marca6" name="marca[]" value="dacia">
                    <label for="marca6">Dacia</label><br>
                    <input type="checkbox" id="marca7" name="marca[]" value="fiat">
                    <label for="marca7">FIAT</label><br>
                    <input type="checkbox" id="marca8" name="marca[]" value="ferrari">
                    <label for="marca8">Ferrari</label><br>
                    <input type="checkbox" id="marca9" name="marca[]" value="ford">
                    <label for="marca9">Ford</label><br>
                    <input type="checkbox" id="marca10" name="marca[]" value="honda">
                    <label for="marca10">Honda</label><br>
                    <input type="checkbox" id="marca11" name="marca[]" value="hyundai">
                    <label for="marca11">Hyundai</label><br>
                    <input type="checkbox" id="marca12" name="marca[]" value="jeep">
                    <label for="marca12">Jeep</label><br>
                    <input type="checkbox" id="marca13" name="marca[]" value="kia">
                    <label for="marca13">KIA</label><br>
                    <input type="checkbox" id="marca14" name="marca[]" value="lancia">
                    <label for="marca14">Lancia</label><br>
                    <input type="checkbox" id="marca15" name="marca[]" value="land rover">
                    <label for="marca15">Land Rover</label><br>
                    <input type="checkbox" id="marca16" name="marca[]" value="mazda">
                    <label for="marca16">Mazda</label><br>
                    <input type="checkbox" id="marca17" name="marca[]" value="mercedes">
                    <label for="marca17">Mercedes</label><br>
                    <input type="checkbox" id="marca18" name="marca[]" value="mini">
                    <label for="marca18">Mini</label><br>
                    <input type="checkbox" id="marca19" name="marca[]" value="mitsubishi">
                    <label for="marca19">Mitsubishi</label><br>
                    <input type="checkbox" id="marca20" name="marca[]" value="nissan">
                    <label for="marca20">Nissan</label><br>
                    <input type="checkbox" id="marca21" name="marca[]" value="opel">
                    <label for="marca21">Opel</label><br>
                    <input type="checkbox" id="marca22" name="marca[]" value="peugeot">
                    <label for="marca22">Peugeot</label><br>
                    <input type="checkbox" id="marca23" name="marca[]" value="renault">
                    <label for="marca23">Abarth</label><br>
                    <input type="checkbox" id="marca24" name="marca[]" value="seat">
                    <label for="marca24">Seat</label><br>
                    <input type="checkbox" id="marca25" name="marca[]" value="skoda">
                    <label for="marca25">Skoda</label><br>
                    <input type="checkbox" id="marca26" name="marca[]" value="suzuki">
                    <label for="marca26">Suzuki</label><br>
                    <input type="checkbox" id="marca27" name="marca[]" value="toyota">
                    <label for="marca27">Toyota</label><br>
                    <input type="checkbox" id="marca28" name="marca[]" value="volkswagen">
                    <label for="marca28">Volkswagen</label><br>
                    <input type="checkbox" id="marca29" name="marca[]" value="volvo">
                    <label for="marca29">Volvo</label><br>
                </div>
                <div id="cambio">
                    <p>Cambio</p>
                    <input type="checkbox" id="cambio1" name="cambio[]" value="manuale">
                        <label for="cambio1">Manuale</label><br>
                    <input type="checkbox" id="cambio2" name="cambio[]" value="automatico">
                        <label for="cambio2">Automatico</label><br>
                </div>
                <input type="hidden" id="hide" name="year" value="" />

                <button type="submit" class="btn" id="apply">Applica Filtri</button>                 
            </form>
        </section>
    </aside>
        
        <main id="lista-auto">
        <?php foreach($templateParams["macchine"] as $car):
            if(!in_array($car["carID"], $_SESSION["carrello"])) {
            ?>
        <article  id="auto<?php echo $car["carID"]; ?>">
            <header>
                <h2 style="text-align: center;"><?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?></h2>
            </header>
            <section>
                <div style="border:none;">
                    <img class="car" src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt="" />
                </div>
                <p>Colore: <?php echo $car["color"]; ?> </p>
                <p>Km: <?php if ($car["kms"] < 1) {
                    echo '0'; }
                    else {
                        echo $car["kms"]; }; ?> - Alimentazione: <?php echo $car["fuelType"]; ?></p>
                <p>Cambio: <?php echo $car["transmission"]; ?></p>
                <p>Motore: <?php echo $car["engine"]; ?></p>
                <p>Prezzo: <?php echo $car["price"]; ?> </p>
                <p>Anno di produzione: <?php echo $car["productionYear"]; ?> - <?php echo $car["carCondition"]; ?> </p>
                <p>Descrizione: <?php echo $car["carDescription"]; ?> </p>
            </section>
            <div id="carrello">
                <button class="btn" data-id="<?php echo $car['carID'] ?>)" id="<?php echo 'id'.$car['carID'] ?>)" >Aggiungi al Carrello <i class="fa fa-cart-plus"></i></button>
            </div>
        </article>
        <?php } endforeach; ?>

        </main>

<script>
    slider();
</script>