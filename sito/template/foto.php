

<main class="noaside">
  <div id="imgcontainer">
    <img src="<?php echo UPLOAD_DIR.$templateParams["img"];?>" alt="" width="80%"/>
  </div>

  <section>
    <h2 style="text-align:center">Chi siamo</h2>
    <p id="presentazione">Noi di GARAGEM ci siamo prefissati di diventare il più grande punto di riferimento per la vendita di automobili nella provincia di Forlì-Cesena.<br>
      Scoprendo i nostri showroom puoi toccare con mano il meglio per quanto concerne la vendita di auto nuove di auto usate, di ogni marca e chilometraggio.<br>
      Rivenditore ufficiale dei marchi più noti nel mercato automobilistico, ci impegnamo nel fornirti un servizio il più completo possibile offrendo auto a Km0, aziendali e di ottima qualità.<br>
      Vieni a trovarci nelle nostre concessionarie a Cesena e Forlì: il nostro team sarà pronto per accompagnarti in ogni fase di acquisto della tua nuova automobile
      e nella scelta di tutte le caratteristiche più adatte a te, anche per quanto riguarda il noleggio, l’assicurazione o la manutenzione.</p>
  </section>

  <section>
    <h2 >Le nostre auto</h2>
    <?php foreach($templateParams["autorandom"] as $car): ?>
      <div>
        <p style="text-align:center;"><?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?> </p>
        <p style="text-align:center;"> Anno <?php echo $car["productionYear"]; ?> ~ KM <?php if ($car["kms"] < 1) {
          echo '0';
        } else {
          echo $car["kms"];
        }; ?></p>
        <img class="car1" src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt=""/>
      </div>
    <?php endforeach; ?>
    <a href="auto.php"><button class="btn" id="view"><i class="fa fa-eye" aria-hidden="true"> Visualizza tutte le auto</i></button></a>
  </section>

  <section>
    <h2 style="text-align:center">Dicono di noi</h2>

      <?php foreach ($templateParams["reviews"] as $review): ?>
        <div class="carousel" style="display:none;">
            <img src="<?php echo UPLOAD_DIR.$review["pic"];?>" alt=""/>
            <h3><?php echo $review["user"]; ?></h3>
            <p><?php echo $review["commento"]; ?></p>
          </div>

        <?php endforeach; ?>

    <script>
      carousel();
    </script>
  </section>
</main>
