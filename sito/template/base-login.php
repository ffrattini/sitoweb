<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <script src="../utils/jquery.js" type="text/javascript"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    <header>
        <img src="https://fontmeme.com/permalink/201207/96e3582d9be5d89b427f064a2ce1604a.png" alt="garagem">
    </header>
    <nav>
        <button class="btn" id="menuButton"><i class="fa fa-bars"></i></button>
        <div id="menu" style="display:none;">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="auto.php">Lista Auto</a></li>
            <li><a href="./sito/carrello.html">Carrello</a></li>
            <li><a href="./template/login.php">Login</a></li>
            <li><a href="./sito/about.html">About</a></li>
        </ul>
        </div>
    </nav>
    <main class="noaside">
    <?php
        if(isset($loginTemplate["main"])){
            require($loginTemplate["main"]);
        }
    ?>
    </main>
    <footer>
      <h2> CONTATTI </h2><br>
      <p> garagem@info.com </p>
      <p> +39 123456789 </p>
    </footer>
</body>
</html>
