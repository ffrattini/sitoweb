<head>
    <style>
        label [for=descrizione], #descrizione {
            vertical-align: top;
        }
        input[type="text"] {
            padding: 2px;
        }
    </style>
</head>

<?php
    unset($_SESSION["msg"]);
    $car = $templateParams["macchine"];
    $azione = getAction($templateParams["azione"]);
?>

<main class="noaside">
    <form action="processa-auto.php" method="POST" enctype="multipart/form-data">
        <h3>Gestisci Auto</h3>
        <?php if($car==null): ?>
            <p>L'auto non è stata trovata</p>
        <?php else: ?>
            <ul id="admin-form">
                <?php if($templateParams["azione"] != 1) : ?>
                <li>
                    <label for="id">ID Auto:</label><input type="text" id="id" name="id" value="<?php echo $car["carID"]; ?>"/>
                </li>
                <?php endif; ?>
                <?php if($templateParams["azione"] != 1) : ?>
                <li>
                    <label for="nomebrand">Marchio/Brand:</label><input type="text" id="nomebrand" name="nomebrand" value="<?php echo $car["brandName"]; ?>" readonly/>
                </li>
                <?php endif; ?>
                <li>
                    <label for="modello">Modello:</label><input type="text" id="modello" name="modello" value="<?php echo  $car["model"]; ?>" />
                </li>
                <li>
                    <label for="colore">Colore:</label><input type="text" id="colore" name="colore" value="<?php echo $car["color"]; ?>" />
                </li>
                <li>
                    <label for="kms">Km:</label><input type="text" id="kms" name="kms" value="<?php echo  $car["kms"]; ?>" />
                </li>
                <li>
                    <label for="alimentazione">Alimentazione:</label><input type="text" id="alimentazione" name="alimentazione" value="<?php echo $car["fuelType"]; ?>" />
                </li>
                <li>
                    <label for="cambio">Cambio:</label><input type="text" id="cambio" name="cambio" value="<?php echo $car["transmission"]; ?>" />
                </li>
                <li>
                    <label for="motore">Motore:</label><input type="text" id="motore" name="motore" value="<?php echo $car["engine"]; ?>" />
                </li>
                <li>
                    <label for="prezzo">Prezzo:</label><input type="text" id="prezzo" name="prezzo" value="<?php echo $car["price"]; ?>" /><span id="prezzo">€</span>
                </li>
                <li>
                <label for="annodiproduzione">Anno di produzione:</label><input type="text" id="annodiproduzione" name="annodiproduzione" value="<?php echo  $car["productionYear"]; ?>" />
                </li>
                <li>
                    <label for="condizione">Condizione:</label><input type="text" id="condizione" name="condizione" value="<?php
                    if($car["carCondition"] == "u" || $car["carCondition"] == "used"): ?>usata
                        <?php elseif($car["carCondition"] == "n" || $car["carCondition"] == "new"): ?>nuova
                        <?php else: echo $car["carCondition"];
                        endif; ?>" />
                </li>
                <li>
                <label for="descrizione">Descrizione:</label><textarea id="description" name="descrizione"><?php
                if(isset($car["carDescription"])) {
                    echo $car["carDescription"]; }
                else {
                    echo 'Sample Text';} ?></textarea>
                </li>
                <li>
                    <?php if($templateParams["azione"]!=3): ?>
                    <label for="image">Immagine macchina</label><input type="file" name="image" id="image" />
                    <?php endif; ?>
                    <?php if($templateParams["azione"]!=1): ?>
                    <img src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt="" width="70%" />
                    <?php endif; ?>
                </li>

                <?php if($templateParams["azione"]!=1): ?>
                <li>
                    <label for="issold">Venduta: </label><input type="text" id="issold" name="issold" value="<?php
                    if($car["isSold"] == 1): ?>SI<?php elseif($car["isSold"] == 0): ?>NO<?php else: ?> <?php endif; ?>" />
                    </li>
                    <li>
                        <label for="orderid"># Ordine: </label><input type="text" id="orderid" name="orderid" value="<?php 
                        if(isset($car["orderID"])) {
                            echo $car["orderID"]; }
                        else {
                            echo " "; } ?>" readonly />
                </li>
                <?php endif; ?>

                <li>
                        <p>Marchio auto</p>
                        <?php foreach($templateParams["brand"] as $brand): ?>
                        <input type="radio" id="<?php echo $brand["brandID"]; ?>" name="marca" value="<?php echo $brand["brandID"]; ?>"
                        <?php
                            if(strcasecmp($brand["brandID"], $car["brandID"]) == 0) {
                                echo ' checked="checked" ';
                            }; ?> />
                            <label for="<?php echo $brand["brandID"]; ?>"><?php echo $brand["brandName"]; ?></label><br>
                        <?php endforeach; ?>
                </li>

                <li>
                    <button type="submit" class="btn" name="submit" value="<?php echo $azione; ?> Macchina" ><i class="fa fa-check-circle" aria-hidden="true"> Applica Modifiche</i></button>
                    <a href="profilo.php"><button type="button" class="btn" id="annulla"><i class="fa fa-undo" aria-hidden="true"> Annulla</i></button></a>
                </li>
            </ul>

            <?php if($templateParams["azione"]!=1): ?>
                <input type="hidden" name="carID" value="<?php echo $car["carID"]; ?>" />
                <input type="hidden" name="brand" value="<?php echo implode(",", $car["brandID"]); ?>" />
                <input type="hidden" name="oldimage" value="<?php echo $car["image"]; ?>" />
            <?php endif;?>

            <input type="hidden" name="action" value="<?php echo $templateParams["azione"]; ?>" />
        <?php endif;?>
    </form>
</main>
