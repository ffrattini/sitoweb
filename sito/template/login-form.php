    <main class="noaside" width="50%">

        <section>
            <form class="login" action="../sito/login.php" method="POST">
            <?php if(isset($templateParams["errorelogin"])): ?>
            <p><?php echo $templateParams["errorelogin"]; ?></p>
            <?php endif; ?>
                <fieldset>
                    <legend style="font-size: 22px; font-weight:bold;">Accedi</legend>
                        <label>E-mail:
                            <input type="email" name="email"
                            autocomplete="off" placeholder="E-mail"
                            required/>
                        </label><br>
                        <label>Password:
                            <input type="password" name="password"
                            autocomplete="off" placeholder="Password"
                            required/>
                        </label>
                </fieldset>
                <button type="submit" class="btn" id="login">Invia</button>
            </form>
            <form class="login" action="register.php" method="POST">
                <h4>Non possiedi un account?<br>Registrati ora, ti bastano pochi secondi!</h4>
                <fieldset>
                    <legend style="font-size: 22px; font-weight:bold;">Registrati</legend>
                        <label>Nome:
                            <input type="text" name="name"
                            autocomplete="off" placeholder="Nome Utente"
                            required/></label>
                            <span class="sr-only">User Name</span><br>
                        <label>E-mail:
                            <input type="email" name="email"
                            autocomplete="off" placeholder="E-mail"
                            required/></label><br>
                        <label>Password:
                            <input type="password" name="password"
                            autocomplete="off" placeholder="Almeno 6 caratteri"
                            required/></label>
                </fieldset>
                <button type="submit" class="btn" id="login">Invia</button>
            </form>

        </section>

    </main>