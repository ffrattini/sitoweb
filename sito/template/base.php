<!DOCTYPE html>
<html lang="it">
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title><?php echo $templateParams["titolo"]; ?></title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="/sitoweb/sito/utils/jquery.js" type="text/javascript"></script>
    <script src="/sitoweb/sito/utils/functions.js" type="text/javascript"></script>
</head>

<body>
    <header>
        <a href="index.php"><img src="https://fontmeme.com/permalink/201207/96e3582d9be5d89b427f064a2ce1604a.png" alt="garagem"></a>
    </header>
    <nav>

    <?php if(isUserLoggedIn()): ?>
            <a href="logout.php"><button class="btn" id="logout" style="float:right;">Logout</button></a>
    <?php endif; ?>

    <button class="btn" id="menuButton" title="apri menu"><i class="fa fa-bars"></i></button>
        <div id="menu" style="display:none;">
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="auto.php">Lista Auto</a></li>
            <li><a href="login.php">Login</a></li>
            <li><a href="profilo.php">Il Mio Profilo</a></li>
            <li><a href="cart.php">Carrello</a></li>
        </ul>
    </div>

    </nav>

    <?php
        if(isset($templateParams["main"])){
            require($templateParams["main"]);
        }
        if(isset($loginTemplate["main"])){
            require($loginTemplate["main"]);
        }
    ?>

    <footer>
    <div style="float:left;">
      <h2> CONTATTI </h2><br>
      <p> garagem@info.com </p>
      <p> +39 123456789 </p>
    </div>
    <div style="float: right;">
        <p>© Copyright 2020 GARAGEM</p>
        <p>All rights reserved</p>
    </div>
    </footer>
</body>

</html>
