<main class="noaside">
    <article id="" style="padding: 10px;">
        <header style="text-align:center;">
            <h1>Grazie per averci scelto!</h1>
            <h3>Questo è il riepilogo del Suo ordine</h3>
        </header>
        <section>
                <p>ID ordine: <?php echo $templateParams["latestOrder"]["orderID"]; ?> </p>
                <p>Data: <?php echo $templateParams["latestOrder"]["orderDate"]; ?> </p>
                <p>E-mail utente: <?php echo $templateParams["latestOrder"]["e_mail"]; ?></p>
        </section>
        </article>
    <?php foreach($templateParams["macchine"] as $car):
        ?>
    <article id="auto<?php echo $car["carID"]; ?>">
        <header>
            <h2 style="text-align: center;"><?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?></h2>
        </header>
        <section>
            <div>
                <img class="car1" src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt="" />
            </div>
            <p>Colore: <?php echo $car["color"]; ?> </p>
            <p>Km: <?php echo $car["kms"]; ?> - Alimentazione: <?php echo $car["fuelType"]; ?></p>
            <p>Cambio: <?php echo $car["transmission"]; ?></p>
            <p>Motore: <?php echo $car["engine"]; ?></p>
            <p> Prezzo: <?php echo $car["price"]; ?> </p>
            <p> Anno di produzione: <?php echo $car["productionYear"]; ?> - <?php echo $car["carCondition"]; ?> </p>
            <p> Descrizione: <?php echo $car["carDescription"]; ?> </p>
        </section>
    </article>
    <?php endforeach; ?>
</main>
