<main>
<?php
if(!empty($_SESSION['carrello'])) {
    
      foreach(array_values($_SESSION['carrello']) as $id):
        foreach($dbh->getCarById($id) as $car):
            ?>
        <article id="auto<?php echo $car["carID"]; ?>">
            <header>
                <h2 style="text-align: center;"><?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?></h2>
                <div>
                    <img class="car" src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt="" />
                </div>
            </header>
            <section>
                <p>Colore: <?php echo $car["color"]; ?> </p>
                <p>Km: <?php echo $car["kms"]; ?> - Alimentazione: <?php echo $car["fuelType"]; ?></p>
                <p>Cambio: <?php echo $car["transmission"]; ?></p>
                <p>Motore: <?php echo $car["engine"]; ?></p>
            </section>
            <div id="rimuovi">
                <button class="btn" data-id="<?php echo $car['carID'] ?>)" id="<?php echo 'cart'.$car['carID'] ?>)">Rimuovi articolo </button>
            </div>

        </article>
<?php endforeach; endforeach;
} if(empty($_SESSION['carrello'])): ?>
    <section>
        <h2>Il tuo carrello è ancora vuoto, visita il nostro catalogo auto per fare acquisti!</h2>
        <a href="auto.php"><button class="btn" id="prof">Vai a Lista Auto</button></a>
</section>
<?php endif; ?>

</main>
<?php
if(!empty($_SESSION['carrello'])) {  ?>
<aside id="completa">
        <button class="btn" id="button-completa"></i>Completa ordine e paga</button>
        <section>

        </section>
</aside>
<?php  } ?>
