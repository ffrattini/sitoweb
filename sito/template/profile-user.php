<aside class="img">
  <img src="<?php echo UPLOAD_DIR.$templateParams["img"]; ?>" style="width: 75%; padding:20px;" />
</aside>

<main>
        <h1 style="text-align:center;">La mia pagina personale</h1>
        <section style="margin-top:20px;">
            <button class="btn" id="accountdetails"><i class="fa fa-user"> Dettagli Account</i></button>
            <div id="menu1" style="display:none;" >
                <?php $user = $templateParams["userinfo"][0]; ?>
                  <p style="font-weight:bold;">Nome: <label style="font-weight:initial;"> <?php echo $user["name"]; ?></label></p>
                  <p style="font-weight:bold;">E-Mail: <label style="font-weight:initial;"><?php echo $user["e_mail"]; ?></label></p>
            </div>

            <button class="btn" id="myorders"><i class="fa fa-first-order"> I miei ordini</i></button>
            <div id="menu2" style="display:none;">
                <?php foreach($templateParams["orderinfo"] as $order): ?>
                  <p style="font-weight:bold;">ID Ordine: <label style="font-weight:initial;"><?php echo $order["orderID"]; ?></label></p>
                  <p style="font-weight:bold;">Data Ordine: <label style="font-weight:initial;"><?php echo $order["orderDate"]; ?></label></p>
                  <p style="font-weight:bold;">Marca auto: <label style="font-weight:initial;"><?php echo $order["brandName"]; ?></label></p>
                  <p style="font-weight:bold;">Modello: <label style="font-weight:initial;"><?php echo $order["model"]; ?></label></p>
                  <p style="font-weight:bold;">Colore: <label style="font-weight:initial;"><?php echo $order["color"]; ?></label></p>
                  <p style="font-weight:bold;">Motore: <label style="font-weight:initial;"><?php echo $order["engine"]; ?></label></p>
                  <p style="font-weight:bold;">Tipo Cambio: <label style="font-weight:initial;"><?php echo $order["transmission"]; ?></label></p>
                  <p style="font-weight:bold;">Alimentazione: <label style="font-weight:initial;"><?php echo $order["fuelType"]; ?></label></p>
                  <p style="font-weight:bold;">Prezzo: <label style="font-weight:initial;"><?php echo $order["price"]; ?> €</label></p>
                  <p style="font-weight:bold;">Descrizione auto: <label style="font-weight:initial;"><?php echo $order["carDescription"]; ?></label></p>
                <?php endforeach; ?>
              <?php if($templateParams["orderinfo"] == null) : ?>
                <p> Nessun ordine è stato effettuato </p>
              <?php endif; ?>
            </div>

        <button class="btn" id="notification"><i class="fa fa-bell"> Notifiche</i></button>
          <div id="menu3" style="display:none;">
            <?php foreach($templateParams["notifiche"] as $notification): ?>
              <div class="notification">
                <span class="chiudi" id="<?php echo 'close'.$notification["notificationID"]; ?>" data-id="<?php echo $notification["notificationID"]; ?>">&times;</span>
                <strong><?php echo $notification["notificationDate"];?></strong><br>
                <?php echo $notification["notificationText"];?>
              </div>
            <?php endforeach; ?>
          <?php if($templateParams["notifiche"] == null) : ?>
            <p> Nessuna notifica da visualizzare </p>
          <?php endif; ?>
          </div>
        </section>

</main>  
