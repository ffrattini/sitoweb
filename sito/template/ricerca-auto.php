<script src="../sito/utils/functions.js"></script>

<main class="noaside">

    <a href="auto.php"><button class="btn" id="return"><i class="fa fa-arrow-left" aria-hidden="true"> Visualizza tutte le auto</i></button></a>
    <?php if(!empty($templateParams["risultato"])): ?>
        <h1 class="h1">Risultati Ricerca per "<?php echo $templateParams["string"]; ?>"</h1></div>
        <?php foreach($templateParams["risultato"] as $car):
            if(!in_array($car["carID"], $_SESSION["carrello"])) {
            ?>
        <article  id="auto<?php echo $car["carID"]; ?>">
            <header>
                <h2 style="text-align: center;"><?php echo $car["brandName"]; ?> <?php echo $car["model"]; ?></h2>
            </header>
            <section>
                <div style="border:none;">
                    <img class="car" src="<?php echo UPLOAD_DIR.$car["image"]; ?>" alt="" />
                </div>
                <p>Colore: <?php echo $car["color"]; ?> </p>
                <p>Km: <?php if ($car["kms"] < 1) {
                    echo '0'; }
                    else {
                        echo $car["kms"]; }; ?> - Alimentazione: <?php echo $car["fuelType"]; ?></p>
                <p>Cambio: <?php echo $car["transmission"]; ?></p>
                <p>Motore: <?php echo $car["engine"]; ?></p>
                <p>Prezzo: <?php echo $car["price"]; ?> </p>
                <p>Anno di produzione: <?php echo $car["productionYear"]; ?> - <?php echo $car["carCondition"]; ?> </p>
                <p>Descrizione: <?php echo $car["carDescription"]; ?> </p>
            </section>
            <div id="carrello">
                <button class="btn" data-id="<?php echo $car['carID'] ?>)" id="<?php echo 'id'.$car['carID'] ?>)" >Aggiungi al Carrello <i class="fa fa-cart-plus"></i></button>
            </div>
        </article>
        <?php } endforeach; ?>
            
    <?php else: ?>
    <div style="border:none;">
        <h1 style="text-align:center; margin-bottom:20px;">Nessun Risultato trovato!</h1>
    </div>
    <?php endif; ?>
</main>