$(document).ready(function(){
    $('[id^="id"]').on('click', function(){
        let carID = $(this).data("id");
        carID = carID.slice(0,-1);
        let auto = "auto";
        let articleID = auto.concat(carID);
        $.ajax({
            method: "POST",
            url: "../sito/add-to-cart.php",
            data: {
                value: carID
            },
            success: function(msg){
                if(msg=="Error"){
                    $('#' + articleID).append("Accedi o registrati per poter aggiungere articoli al carrello.");
                } else {
                    $('#' + articleID).remove();
                }
            }
        })
    });

    $('[id^="cart"]').on('click', function(){
        let carID = $(this).data("id");
        carID = carID.slice(0,-1);
        $.ajax({
            method: "POST",
            url: "../sito/remove-from-cart.php",
            data: {
                value: carID
            }
        })
        let auto = "auto";
        let articleID = auto.concat(carID);
        $('#' + articleID).remove();
    });

    $("#button-completa").on('click', function(){
        window.location = "../sito/order-complete.php";
    });

    $('[id^="close"]').click(function() {
        var div = this.parentElement;
        div.style.opacity = "0";
        setTimeout(function(){ div.style.display = "none"; }, 600);
        let notificationID = $(this).data("id");
        $.ajax({
            method: "POST",
            url: "../sito/mark-as-read.php",
            data: {
                value: notificationID
            }
        })
    });
});

var slideIndex = 0;

function carousel() {
    var i;
    var x = document.getElementsByClassName("carousel");
    for (i = 0; i < x.length; i++) {
      x[i].style.display = "none";
    }
    slideIndex++;
    if (slideIndex > x.length) {slideIndex = 1}
    x[slideIndex-1].style.display = "block";
    setTimeout(carousel, 4000);
}

function closenotification() {
    var close = document.getElementsByClassName("chiudi");
    var i;

}

function slider() {
    var slider = document.getElementById("anno");
    var output = document.getElementById("demo");
    output.innerHTML = slider.value;
    $("#hide").val(slider.value)
  
    slider.oninput = function() {
      output.innerHTML = this.value;
      $("#hide").val(this.value);
    }
}
