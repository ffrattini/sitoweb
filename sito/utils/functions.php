<?php

require_once("bootstrap.php");

function startSecureSession(){
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], false, true); 
    session_name('secure-session');
    session_start(); 
    session_regenerate_id(); 
}

function registerLoggedUser($user){
    $_SESSION["e_mail"] = $user["e_mail"];
    $_SESSION["name"] = $user["name"];
}

function checkLogin($e_mail, $password, $dbE_mail, $hashedPassowrd){
    return ($e_mail == $dbE_mail && password_verify($password, $hashedPassowrd));
}

function isUserLoggedIn(){
    return !empty($_SESSION["e_mail"]);
}

function logCurrentUserOut(){
    unset($_SESSION["e_mail"]);
    unset($_SESSION["name"]);
    unset($_SESSION["carrello"]);
}

function getAction($action){
    $result = "";
    switch($action){
        case 1:
            $result = "Inserisci";
            break;
        case 2:
            $result = "Modifica";
            break;
        case 3:
            $result = "Cancella";
            break;
    }
    return $result;
}

function getAccountDetails($mail){
    $query_result = $dbh->getAccountInfo($mail);
    return $query_result;
}

function getOrderDetails(){
  $query_result = $dbh->getOrderInfo($_SESSION["e_mail"]);
  return $query_result;
}

function getEmptyCar(){
    return array("carID" => "", "model" => "", "color" => "", "kms" => "", "transmission" => "", "engine" => "",
    "fuelType" => "", "price" => "","carCondition" => "","productionYear" => "","brandID" => "", "brandName" => "", "carDescription" => "Sample text" , "isSold"  => "null");
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path.$imageName;

    $maxKB = 500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg = "Errore nel caricamento dell'immagine.";
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
  }
?>
