$(document).ready(function(){

  $("button#menuButton").click(function(){
    $("div#menu").slideToggle();
  });

  $("button#filter").click(function(){
    $("#filtro").slideToggle();
  });

  $("button#accountdetails").click(function(){
    $("div#menu1").slideToggle();
  });

  $("button#myorders").click(function(){
    $("div#menu2").slideToggle();
  });

  $("button#allorders").click(function(){
    $("div#menu4").slideToggle();
  });

  $("button#notification").click(function(){
    $("div#menu3").slideToggle();
  });

  $("input#search").on('focus', function () {
    $(this).parent('label').addClass('active');
  });
  
  $("input#search").on('blur', function () {
    if($(this).val().length == 0)
      $(this).parent('label').removeClass('active');
  });

});
