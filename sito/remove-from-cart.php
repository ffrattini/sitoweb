<?php
require_once("bootstrap.php");

if(!isset($_SESSION['carrello'])){
    $_SESSION['carrello'] = array();
}

if (($key = array_search($_POST["value"], $_SESSION['carrello'])) !== false) {
    unset($_SESSION['carrello'][$key]);
}

?>