<?php
require_once("bootstrap.php");
require_once("notifiche.php");

$dbh->createOrder($_SESSION["e_mail"]);
$latestOrder = $dbh->getLatestOrder($_SESSION["e_mail"]);
$dbh->sellCars($_SESSION["carrello"], $latestOrder[0]["orderID"]);
$text = getNotificationTextType(3);
$dbh->createNotification($_SESSION["e_mail"], $text, 3);
$text = getNotificationTextType(6);
$templateParams["adminmail"] = $dbh->sendAdminNotification();
$dbh->createNotification($templateParams["adminmail"][0]["e_mail"], $text, 7);

$templateParams["titolo"] = "Garagem - Il tuo ordine";
$templateParams["main"] = "template/latest-order.php";
$templateParams["latestOrder"] = $latestOrder[0];
$templateParams["macchine"] = $dbh->getCarsByOrderID($latestOrder[0]["orderID"]);

unset($_SESSION['carrello']);

//DA SISTEMARE
// aspetto cinque minuti prima di mandare la notifica di informazione auto pronta
$text = getNotificationTextType(4);
$dbh->createNotification($_SESSION["e_mail"], $text, 4);

require "template/base.php";
?>




 }
