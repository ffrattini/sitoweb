<?php
require_once("bootstrap.php");

$templateParams["titolo"] = "Garagem - Lista Auto";
$templateParams["macchine"] = $dbh->getListaAuto();
$templateParams["main"] = "template/lista-auto.php";
$templateParams["ricerca"] = $dbh->searchBar();

$idauto = -2;
if(isset($_GET["id"])){
    $idauto = $_GET["id"];
}
$templateParams["id"] = $dbh->getCarById($idauto);


if(!isset($_SESSION['carrello'])){
    $_SESSION['carrello'] = array();
}

require "template/base.php";
?>
