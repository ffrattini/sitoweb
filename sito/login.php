<?php
require_once("bootstrap.php");

if(isset($_POST["email"])){
    if(!$dbh->canAttemptLogin($_POST["email"])){
        $templateParams["errorelogin"] = "Troppi tentativi di accesso falliti, contatta l'amministratore per il reset della password";
    } else {
        if(isset($_POST["email"]) && isset($_POST["password"])){
            $credentials = $dbh->getCredentials($_POST["email"]);
            if(count($credentials)==1 && checkLogin($_POST["email"], $_POST["password"], $credentials[0]["e_mail"], $credentials[0]["password"])){
                    registerLoggedUser($credentials[0]);
                    $dbh->resetAttempts($_POST["email"]);
            } else {
                    $templateParams["errorelogin"] = "Errore nel login! Verificare che le credenziali siano corrette.";
                    $dbh->increaseAttempts($_POST["email"]);
            }
        }
    }
}

if(isUserLoggedIn()){
    $templateParams["titolo"] = "Login";
    $loginTemplate["main"] = "login-home.php";
}
else{
    $templateParams["titolo"] = "Garagem - Accedi o Registrati";
    $loginTemplate["main"] = "login-form.php";
}

require("template/base.php");


?>
