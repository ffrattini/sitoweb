<?php
        require_once("bootstrap.php");

        $templateParams["titolo"] = "Garagem - Risultato della Ricerca";
        if (isset($_POST["marca"])) {
                $templateParams["brand"] = implode("','", $_POST["marca"]);
                $templateParams["brand"] = "'".$templateParams["brand"]."'";
        } else {
                $templateParams["brand"] = "b.brandName";
        }
        if (isset($_POST["cambio"])) {
                $templateParams["transmission"] = implode(" ", $_POST["cambio"]);
                $templateParams["transmission"] = str_replace(" ", "%' OR c.transmission LIKE '", $templateParams["transmission"]);
                $templateParams["transmission"] = "'".$templateParams["transmission"]."%'";
        } else {
                $templateParams["transmission"]="'%'";
        }
        if (isset($_POST["carburante"])) {
                $templateParams["alimentazione"] = implode("','", $_POST["carburante"]);
                $templateParams["alimentazione"] = "'".$templateParams["alimentazione"]."'";
        } else {
                $templateParams["alimentazione"] = "c.fuelType";
        }

        $templateParams["anno"] = $_POST["year"];


        $templateParams["risultato"]=$dbh->filterResult($templateParams["brand"], $templateParams["transmission"], $templateParams["alimentazione"], $templateParams["anno"]);   
        $templateParams["main"] = "template/filtri-auto.php"; 

        require("template/base.php");         

?>