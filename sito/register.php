<?php
    require_once("bootstrap.php");
    require_once("notifiche.php");

    if(isset($_POST["name"]) && isset($_POST["password"]) && isset($_POST["email"])){
        $dbh->registerNewUser(password_hash($_POST["password"], PASSWORD_BCRYPT), $_POST["email"], $_POST["name"]);
        $text = getNotificationTextType(1);
        $dbh->createNotification($_POST["email"], $text, 1);
        $text = getNotificationTextType(7);
        $templateParams["adminmail"] = $dbh->sendAdminNotification();
        $dbh->createNotification($templateParams["adminmail"][0]["e_mail"], $text, 7);
    } else {
    }

    require("login.php");
?>
