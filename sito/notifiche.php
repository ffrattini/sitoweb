<?php
require_once("bootstrap.php");

function getNotificationTextType($notificationType){
    $text = "";
    switch($notificationType){
        case 1:
            $text = "Grazie per esserti registrato al nostro sito!
                     Con noi sarai sempre aggiornato sulle promozioni ed offerte più convenienti della zona.";
            break;
        case 2:
            $text = "Una nuova auto è stata aggiunta, non fartela scappare!
                     Corri subito a scoprire le nostre proposte nella lista auto.";
            break;
        case 3:
            $text = "Grazie per aver comprato da noi!
                     Il tuo ordine è ora in lavorazione e verrai avvisato non appena saremo pronti per il ritiro.";
            break;
        case 4:
            $text = "La tua auto è pronta!
                     Da oggi potrai venire in concessionaria per ritirarla in ogni momento.
                     Contattaci per eventuali comunicazioni o/e problemi.";
            break;
        case 5:
            $text = "La scheda tecnica di un'auto è appena stata modificata!
                     Corri a guardare la nostra lista auto per rimanere aggiornato e non perderti nessuna novità.";
            break;
        case 6:
            $text = "Un nuovo ordine è appena stato effettuato!
                     Controlla tutti i dettagli per assistere il cliente al meglio.";
            break;
        case 7:
            $text = "Buona notizia! Un nuovo utente si è appena registrato al sito.";
            break;
        case 8:
            $text = "Un'auto è appena stata cancellata dalla nostra lista perchè non più disponibile o è stata venduta.";
            break;
    }
    return $text;
}
?>
