<?php
require_once("bootstrap.php");

if(!isset($_SESSION['carrello'])){
    $_SESSION['carrello'] = array();
}
if(isUserLoggedIn()){
    array_push($_SESSION['carrello'], $_POST["value"]);
    echo "Ok";
} else {
    echo "Error";
}
?>