<?php
require_once("bootstrap.php");

$templateParams["img"] = "fuel-meter-311685_1280.png";

// se l'utente è loggato
if (isUserLoggedIn()){
  $templateParams["userinfo"] = $dbh->getAccountInfo($_SESSION["e_mail"]);
  $templateParams["orderinfo"] = $dbh->getOrderInfo($_SESSION["e_mail"]);
  $templateParams["allordersinfo"] = $dbh->getAllOrdersInfo();
  $templateParams["notifiche"] =  $dbh->getNotReadNotificationByUser($_SESSION["e_mail"]);
  $templateParams["allUsersNotification"] = $dbh->sendAllUserNotification();

  if(isset($_GET["formmsg"])){
      $templateParams["formmsg"] = $_GET["formmsg"];
  }
  $templateParams["macchine"] = $dbh->getListaAuto();

  if($dbh->checkAdmin($_SESSION["e_mail"])){
    $templateParams["titolo"] = "Profilo Amministratore";
    $templateParams["main"] = "profile-admin.php";
  }
  else{
    $templateParams["titolo"] = "Garagem - Profilo Personale";
    $templateParams["main"] = "profile-user.php";
  }
// se l'utente non è loggato viene mostrata una scritta
} else {
    $templateParams["titolo"] = "GARAGEM - Accedi o Registrati";
   $templateParams["errorelogin"] = "Per visualizzare il tuo profilo devi aver effettuato l'accesso";
   $loginTemplate["main"] = "login-form.php";
}

require("template/base.php");
?>
