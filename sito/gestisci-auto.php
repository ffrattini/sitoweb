<?php
require_once "bootstrap.php";

if(!isUserLoggedIn() || !isset($_GET["action"]) || ($_GET["action"]!=1 && $_GET["action"]!=2 && $_GET["action"]!=3) || ($_GET["action"]!=1 && !isset($_GET["id"]))){
  $loginTemplate["titolo"] = "Garagem";
  $loginTemplate["main"] = "login-form.php";
}

$templateParams["brand"] = $dbh->getBrandList();

if($_GET["action"]!=1){
    $risultato = $dbh->getCarById($_GET["id"]);
    if(count($risultato) < 0){
        $templateParams["macchine"] = null;
    }
    else{
        $templateParams["macchine"] = $risultato[0];
    }
}
else{
    $templateParams["macchine"] = getEmptyCar();
}

$templateParams["titolo"] = "Garagem - Modifica Auto";
$templateParams["main"] = "admin-form.php";

$templateParams["azione"] = $_GET["action"];

require("template/base.php");
?>
