<?php
require_once("bootstrap.php");


if(isUserLoggedIn()) {
    $templateParams["titolo"] = "Il mio Carrello";
    $templateParams["main"] = "template/carrello.php";
} else {
    $templateParams["errorelogin"] = "Per visualizzare il tuo carrello, accedi o registrati.";
    $templateParams["titolo"] = "Garagem - Accedi o Registrati";
    $loginTemplate["main"] = "login-form.php";
}


require 'template/base.php';
?>