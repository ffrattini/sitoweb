<?php
        require_once("bootstrap.php");

        if(isset($_POST["ricerca"])) {
            $templateParams["titolo"] = "Garagem - Risultato della Ricerca";
            $templateParams["risultato"]=$dbh->searchBar("%".$_POST["ricerca"]."%");   
            $templateParams["main"] = "template/ricerca-auto.php";
            $templateParams["string"] = $_POST["ricerca"];
            require("template/base.php");         
        } else {
            require("auto.php");
        }
?>