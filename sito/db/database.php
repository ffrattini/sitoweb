<?php

class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if($this->db->connect_error){
            die("Connessione fallita al db");
        }
    }

// -----------------------   USERS AND ADMIN --------------------

    public function checkLogin($e_mail, $password){
        $stmt = $this->db->prepare("SELECT e_mail, name FROM user WHERE e_mail = ? AND password = ?");
        $stmt->bind_param("ss", $e_mail, $password);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCredentials($e_mail){
        $stmt = $this->db->prepare("SELECT e_mail, password, name FROM user WHERE e_mail = ?");
        $stmt->bind_param("s", $e_mail);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function registerNewUser($password, $e_mail, $name){
        $query = "INSERT INTO `user` (`password`, `e_mail`, `name`, `isAdmin`) VALUES (?, ?, ?, 0);";
        $stmt = $this->db->prepare("INSERT INTO `user` (`password`, `e_mail`, `name`, `isAdmin`) VALUES (?, ?, ?, 0);");
        $stmt->bind_param("sss", $password, $e_mail, $name);
        $stmt->execute();
    }

    public function checkAdmin($e_mail){
        $stmt = $this->db->prepare("SELECT e_mail FROM user WHERE e_mail = ? AND isAdmin = 1");
        $stmt->bind_param("s", $e_mail);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function isUserRegistered($user){
        $stmt = $this->db->prepare("SELECT e_mail FROM user WHERE e_mail = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
        $result = $result->fetch_all(MYSQLI_ASSOC);
        return (count($result)==1);
    }

    public function increaseAttempts($user){
        if($this->isUserRegistered($user)){
            $query = "UPDATE `user` SET `attempts`= `attempts`+ 1 WHERE `e_mail` = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("s", $user);
            $stmt->execute();
        }
        
    }

    public function getAttempts($user){
        $stmt = $this->db->prepare("SELECT attempts FROM user WHERE e_mail = ?");
        $stmt->bind_param("s", $user);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function resetAttempts($user){
        $query = "UPDATE `user` SET `attempts`= 0 WHERE `e_mail` = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $user);
        $stmt->execute();
    }
    
    public function canAttemptLogin($user){
        if($this->isUserRegistered($user)){
            return $this->getAttempts($user)[0]["attempts"] < 3;
        } else{
            return true;
        }   
    }

// -----------------------   PERSONAL ACCOUNT --------------------

    public function getAccountInfo($e_mail){
      $stmt = $this->db->prepare("SELECT e_mail, name FROM user WHERE e_mail = ?");
      $stmt->bind_param("s", $e_mail);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

//  -----------------------   ORDERS --------------------

    public function createOrder($e_mail){
        $query = "INSERT INTO `order_details` (`orderID`, `orderDate`, `e_mail`) VALUES ('0', CURDATE(), ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $e_mail);
        $stmt->execute();
    }

    public function sellCars($cars, $orderID){
        foreach ($cars as &$value) {
            $query = "UPDATE `car` SET `orderID`= ? WHERE carID = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("is", $orderID, $value);
            $stmt->execute();

            $query = "UPDATE `car_sale_info` SET `isSold`= 1 WHERE carID = ?";
            $stmt = $this->db->prepare($query);
            $stmt->bind_param("i", $value);
            $stmt->execute();
        }
    }

    public function getLatestOrder($e_mail){
      $stmt = $this->db->prepare("SELECT * FROM `order_details` WHERE e_mail = ? order by orderID desc limit 1");
      $stmt->bind_param("s", $e_mail);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getOrderInfo($e_mail){
      $stmt = $this->db->prepare("SELECT * FROM order_details o, car c, brand b, car_sale_info cs WHERE e_mail = ? AND o.orderID = c.orderID AND c.carID = cs.carID AND c.brandID = b.brandID");
      $stmt->bind_param("s", $e_mail);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getAllOrdersInfo(){
      $stmt = $this->db->prepare("SELECT * FROM order_details o, car c, brand b, car_sale_info cs, user u WHERE o.orderID = c.orderID AND c.carID = cs.carID AND c.brandID = b.brandID AND u.e_mail = o.e_mail  ORDER BY o.orderID DESC");
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

// ------------------- FILTERS AND SEARCHBAR ---------------

    public function searchBar($string="") {
        if(strlen($string) > 0) {
            $query = $this->db->prepare("SELECT * FROM car c, brand b, car_sale_info cs WHERE CONCAT_WS(c.model, c.color, c.kms, c.transmission, c.engine, c.fuelType, b.brandName) LIKE ? AND c.brandID = b.brandID AND c.carID = cs.carID");
            $query->bind_param('s', $string);
            $query->execute();
            $result = $query->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
    }

    public function filterResult($marca, $cambio, $alimentazione, $anno) {
        $query = $this->db->prepare("SELECT * FROM car c, brand b, car_sale_info cs WHERE (c.transmission LIKE $cambio) AND c.fuelType IN ($alimentazione) AND b.brandName IN ($marca) AND cs.productionYear > ? AND c.carID = cs.carID AND c.brandID = b.brandID");
        $query->bind_param('i', $anno);
        $query->execute();
        $result = $query->get_result();

        return($result->fetch_all(MYSQLI_ASSOC));
    }

// ----------------- CARS AND BRANDS -----------------

    public function getCarsByOrderID($orderID){
        $stmt = $this->db->prepare("SELECT * FROM car c, brand b, car_sale_info cs WHERE c.brandID = b.brandID AND c.carID = cs.carID AND c.orderID = ? ORDER BY c.carID ASC");
        $stmt->bind_param("i", $orderID);
        $stmt->execute();
        $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getListaAuto(){
        $query = "SELECT * FROM car c, brand b, car_sale_info cs WHERE c.brandID = b.brandID AND c.carID = cs.carID AND cs.isSold = 0 ORDER BY c.carID ASC";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getRandomCars($n=2){
        $stmt = $this->db->prepare("SELECT * FROM car c, brand b, car_sale_info cs WHERE c.brandID = b.brandID AND c.carID = cs.carID AND cs.isSold = 0 ORDER BY RAND() LIMIT ?");
        $stmt->bind_param('i', $n);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCartCars($ids){
        $idsString = implode(",", $ids);
        $query = "SELECT * FROM car c, brand b WHERE c.brandID = b.brandID AND c.carID IN (?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("s", $idsString);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCarById($carID){
        $query = "SELECT * FROM car c, brand b, car_sale_info cs WHERE c.carID = ? AND c.brandID = b.brandID AND c.carID = cs.carID";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $carID);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getCarByBrand($brandID){
        $query = "SELECT * FROM car c, brand b, car_sale_info cs WHERE c.brandID = b.brandID = ? AND c.carID = cs.carID";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i',$brandID);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBrand($name){
        $query = $this->db->prepare("SELECT brandID FROM brand WHERE brandName = ?");
        $query->bind_param('s', $name);
        $query->execute();
        $result = $query->get_result();

        var_dump($result);
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function getBrandList() {
        $query=$this->db->prepare("SELECT * FROM brand ORDER BY brandID ASC");
        $query->execute();
        $result=$query->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

// ---------------- CARS MANAGEMENT -------------

    public function insertCar($model, $color, $kms, $transmission, $engine, $fuelType, $brandID){
        $query = $this->db->prepare("INSERT INTO car (model, color, kms, transmission, engine, fuelType, orderID, brandID) VALUES (?, ?, ?, ?, ?, ?, NULL, ?)");
        $query->bind_param("ssisssi", $model, $color, $kms, $transmission, $engine, $fuelType, $brandID);
        $query->execute();

        return $query->insert_id;
    }

    public function insertCarSaleInfo($id, $price, $carCondition, $productionYear, $carDescription="Sample Text", $isSold, $image="peroraniente"){
        $query = $this->db->prepare("INSERT INTO car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, `image`) VALUES (?, ?, ?, ?, ?, ?, ?)");
        $query->bind_param('iisisis', $id, $price, $carCondition, $productionYear, $carDescription, $isSold, $image);
        $query->execute();

        return $query->insert_id;
    }

    public function updateCar($oldcarID, $model, $color, $kms, $transmission, $engine, $fuelType, $orderID, $newcarID, $brandID){
        $query = "UPDATE car SET carID = ?, model = ?, color = ?, kms = ?, transmission = ?, engine = ?, fuelType = ?, orderID = ?, brandID = ? WHERE carID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ississsiii', $newcarID, $model, $color, $kms, $transmission, $engine, $fuelType, $orderID, $brandID, $oldcarID);

        return $stmt->execute();
    }

    public function updateCarSaleInfo($oldcarID, $price, $carCondition, $productionYear, $carDescription, $isSold, $image, $newcarID){
        $query = "UPDATE car_sale_info SET carID = ?, price = ?, carCondition = ?, productionYear = ?, carDescription = ?, isSold = ?, `image` = ? WHERE carID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('iisisisi', $newcarID, $price, $carCondition, $productionYear, $carDescription, $isSold, $image, $oldcarID);
        return $stmt->execute();
    }

    public function deleteCar($carID){
        $query = "DELETE FROM car WHERE carID = ? ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $carID);
        var_dump($stmt->error);
        return true;
    }

    public function deleteCarSaleInfo($carID){
        $query = "DELETE FROM car_sale_info WHERE carID = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $carID);
        return $stmt->execute();
    }

  //  ------------- NOTIFICATIONS --------------

  public function createNotification($userEmail, $notificationText, $notificationType){
      $query = "INSERT INTO notification (notificationID, userEmail, notificationText, isRead, NotificationDate, notificationType) VALUES (0, ?, ?, 0, CURDATE(), ?)";
      $stmt = $this->db->prepare($query);
      $stmt->bind_param("ssi", $userEmail, $notificationText, $notificationType);
      $stmt->execute();
  }

  public function getNotificationByUser($userEmail){
      $stmt = $this->db->prepare("SELECT notificationText, notificationDate FROM notification n, user u WHERE n.userEmail = u.e_mail AND userEmail = ? ORDER BY n.notificationDate DESC");
      $stmt->bind_param('s', $userEmail);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function getNotReadNotificationByUser($userEmail){
      $stmt = $this->db->prepare("SELECT * FROM notification n, user u WHERE n.userEmail = u.e_mail AND n.isRead = 0 AND n.userEmail=?");
      $stmt->bind_param('s', $userEmail);
      $stmt->execute();
      $result = $stmt->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function setNotificationRead($notificationID){
      $query = $this->db->prepare("UPDATE `notification` SET isRead = 1 WHERE notificationID = ? ");
      $query->bind_param('i', $notificationID);
      $query->execute();
      return $query->execute();
  }

  public function sendAdminNotification(){
    $stmt = $this->db->prepare("SELECT e_mail FROM user WHERE isAdmin = 1");
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  public function sendAllUserNotification(){
    $stmt = $this->db->prepare("SELECT e_mail FROM user WHERE user.isAdmin = 0");
    $stmt->execute();
    $result = $stmt->get_result();

    return $result->fetch_all(MYSQLI_ASSOC);
  }

  //  ------------- REVIEWS --------------

  public function GetReviews() {
      $query = $this->db->prepare("SELECT * FROM review");
      $query->execute();
      $result=$query->get_result();

      return $result->fetch_all(MYSQLI_ASSOC);
  }

}
?>
