insert into brand (brandName) values ("Ferrari"), ("Abarth"),
("Alfa Romeo"), ("Audi"), ("BMW"), ("Citroen"), ("Dacia"), ("FIAT"), ("Ford"), ("Honda"), ("Hyundai"), ("Jeep"),
("KIA"), ("Lancia"), ("Land Rover"), ("Mazda"), ("Mercedes"), ("Mini"), ("Mitsubishi"), ("Nissan"),
("Opel"), ("Peugeot"), ("Renault"), ("Seat"), ("Skoda"), ("Suzuki"), ("Toyota"), ("Volkswagen"), ("Volvo");


insert into user (password, e_mail, name, isAdmin) values
("$2y$10$4lO01piybD1hGh84GXTbAOM06/HHGuCYOjgNuZi7Lm8.gMxAo8/Be", "adminsito@gmail.com", "admin sito", 1),
("$2y$10$CxY/3qEF15eU17NAe6KLxOVJhlZ/P0olzWfa9X4Vx5d3hwTRzQ3wG", "prova@gmail.com", "utente prova", 0);


insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('Ka', 'bianco', '20000', 'manuale 5 rapporti', '1.2 8V 69 CV', 'benzina', 26, 9);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(1, '13500', 'usato', '2011', 'Auto ancora in ottimo stato nonostante sia usata e dell\'anno 2011.', 1, 'Ford-ka.jpg');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('Q2', 'bianco', '88382', 'automatico', '85 kW 116 CV', 'diesel', 25, 4);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(2, '20000', 'usato', '2017', 'Auto usata solo per qualche mese, è quindi in condizioni quasi nuove.', 1, 'Audi-Q2.jpg');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('X2', 'nero metallizzato', '-', 'manuale', '103 kW 140 CV', 'benzina', NULL, 5);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(3, '24000', 'nuovo', '2019', 'Auto nuova, suv fuoristrada. NOTE: disponibile 120 giorni dalla data di ordine', 0, 'BMW-X2.jpg');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('Giulia Q', 'Rosso Alfa', '5', 'manuale', '88 kW 120 CV', 'benzina', NULL, 3);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(4, '22000', 'nuovo', '2019', 'Auto in condizioni ottime, i soli 5 km di strada percorsi rendono questa macchina perfetta idonea ad essere considerata nuova.', 0, 'Alfa-Giulietta.png');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('Testarossa', 'Rosso Ferrari', '45000', 'benzina', ' 287 kW 390 CV', 'benzina', NULL, 1);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(5, '120000', 'usato', '1988', 'Auto ferrari testarossa tutta originale tagliandata ferrari cinghie fatte card asi oro scarichi piu originali targata modena come nuova eventuale permuta con ferrari 430', 0, 'Ferrari-Testarossa.jpg');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('500', 'Rosso', '122000', 'Manuale 5 rapporti', '99 kW 135 CV', 'benzina', NULL, 2);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(6, '10000', 'usato', '2008', 'Auto in pronta consegna, kit Esseesse, tagliando e gomme tutto regolare, garanzia 12 mesi, finanziabile', 0, 'Abarth-500.jpg');

insert into car (model, color, kms, transmission, engine, fuelType, orderID, brandID) values
('A4 Avant', 'Grigio', '9500', 'Automatico', '150 kW 204 CV', 'diesel', NULL, 4);
insert into car_sale_info (carID, price, carCondition, productionYear, carDescription, isSold, image) values
(7, '60000', 'usato', '2019', 'Auto usata ma ancora in ottime condizioni. Finanziabile.', 0, 'Audi-A4.png');


insert into order_details (orderID, orderDate, e_mail) values
(1, "2021-01-03", "prova@gmail.com");
UPDATE car SET orderID = 1 WHERE carID = 1;
UPDATE car_sale_info SET isSold = true WHERE carID = 1;

insert into order_details (orderID, orderDate, e_mail) values
(2, "2021-01-03", "prova@gmail.com");
UPDATE car SET orderID = 2 WHERE carID = 2;
UPDATE car_sale_info SET isSold = true WHERE carID = 2;


insert into notification (notificationID, userEmail, notificationText, isRead, notificationDate, notificationType) values
(1, "prova@gmail.com", "Grazie per esserti registrato al nostro sito! Con noi sarai sempre aggiornato sulle promozioni ed offerte più convenienti della zona.", 0, "2021-01-03", 1),
(2, "adminsito@gmail.com", "Buona notizia! Un nuovo utente si è appena registrato al sito.", 0, "2021-01-03", 7),
(3, "prova@gmail.com", "Una nuova auto è stata aggiunta, non fartela scappare! Corri subito a scoprire le nostre proposte nella lista auto.", 0, "2021-01-03", 2),
(4, "prova@gmail.com", "Una nuova auto è stata aggiunta, non fartela scappare! Corri subito a scoprire le nostre proposte nella lista auto.", 0, "2021-01-03", 2),
(5, "prova@gmail.com", "Grazie per aver comprato da noi! Il tuo ordine è ora in lavorazione e verrai avvisato non appena saremo pronti per il ritiro.", 0, "2021-01-03", 3),
(6, "adminsito@gmail.com", "Un nuovo ordine è appena stato effettuato! Controlla tutti i dettagli per assistere il cliente al meglio.", 0, "2021-01-03", 7),
(7, "prova@gmail.com", "La tua auto è pronta! Da oggi potrai venire in concessionaria per ritirarla in ogni momento. Contattaci per eventuali comunicazioni o/e problemi.", 0, "2021-01-03", 4),
(8, "prova@gmail.com", "Grazie per aver comprato da noi! Il tuo ordine è ora in lavorazione e verrai avvisato non appena saremo pronti per il ritiro.", 0, "2021-01-03", 3),
(9, "adminsito@gmail.com", "Un nuovo ordine è appena stato effettuato! Controlla tutti i dettagli per assistere il cliente al meglio.", 0, "2021-01-03", 7),
(10, "prova@gmail.com", "La tua auto è pronta! Da oggi potrai venire in concessionaria per ritirarla in ogni momento. Contattaci per eventuali comunicazioni o/e problemi.", 0, "2021-01-03", 4);


insert into review (user, pic, commento) values
("Mariano Giusti", "Sfondo_recensione_cliente.jpg", "Consiglio vivamente questo autosalone. Ho trovato professionalità, disponibilità, empatia, auto bellissime e soprattutto trasparenza."),
("Leonardo Farina", "Sfondo_recensione_cliente.jpg", "Consiglio questa concessionaria in quanto ho trovato professionalità, qualità, cortesia e cosa che non guasta disponibilità. Prezzi vantaggiosi e disbrigo pratico velocissimo."),
("Mirella Bella", "Sfondo_recensione_cliente.jpg", "Gentilissimi e disponibili. Ho acquistato una Lancia Ypsilon e sono stata seguita in tutto dalla scelta dell'auto a me più adatta fino al ritiro. Consiglio a tutti."),
("Alessandro Rossi", "Sfondo_recensione_cliente.jpg", "Pienamente soddisfatto. Ho acquistato una auto Km Zero ma hanno davvero tante occasioni a prezzi super. Consiglio."),
("Ginevra Mamini", "Sfondo_recensione_cliente.jpg", "È già la seconda auto che acquisto da GARAGEM. In poche parole sono affidabili, precisi e disponibili. Consiglio al 100%!"),
("Giulia Ricci", "Sfondo_recensione_cliente.jpg", "Consigliata dalla scelta dell'auto fino alla procedura di pagamento, considerando sempre quelle che sono le mie esigenze e possibilità. Consiglio questo autosalone a tutti per la serietà, disponibilità, cortesia verso il cliente e soprattutto la qualità delle auto.");
