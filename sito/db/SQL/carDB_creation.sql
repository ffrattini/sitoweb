-- *********************************************
-- * Standard SQL generation
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1
-- * Generator date: Dec  4 2018
-- * Generation date: Wed Dec  2 11:40:45 2020
-- * LUN file: C:\Users\Gian Camion\Desktop\SITOWEB.lun
-- * Schema: SCHEMA LOGICO/SQL1
-- *********************************************


-- Database Section
-- ________________

create database AUTODB;
use AUTODB;


-- DBSpace Section
-- _______________


-- Tables Section
-- _____________

create table BRAND (
     brandID int(8) not null AUTO_INCREMENT,
     brandName char(16) not null,
     constraint ID_BRAND_ID primary key (brandID));

create table CAR (
     carID int(8) not null AUTO_INCREMENT,
     model char(32) not null,
     color char(16) not null,
     kms char(24) not null,
     transmission char(32) not null,
     engine char(32) not null,
     fuelType char(16) not null,
     orderID int(8),
     brandID int(8) not null,
     constraint ID_CAR_ID primary key (carID));

create table CAR_SALE_INFO (
     carID int(8) not null,
     price char(16) not null,
     carCondition char(16) not null,
     productionYear char(4) not null,
     carDescription text,
     isSold boolean not null,
     image char(64),
     constraint FKcar_details_ID primary key (carID));

create table ORDER_DETAILS (
     orderID int(8) not null AUTO_INCREMENT,
     orderDate date not null,
     e_mail char(64) not null,
     constraint ID_ORDER_DETAILS_ID primary key (orderID));

create table USER (
     password char(64) not null,
     e_mail char(64) not null,
     name char(32) not null,
     isAdmin boolean not null,
	 attempts int(8) not null,
     constraint ID_USER_ID primary key (e_mail));

CREATE TABLE REVIEW (
     user char(32) NOT NULL ,
     pic char(64) NOT NULL ,
     commento text NOT NULL,
     constraint ID_USER_ID primary key (user));

CREATE TABLE NOTIFICATION (
     notificationID int(8) not null AUTO_INCREMENT,
     notificationType int(8) not null,
     userEmail char(32) NOT NULL ,
     notificationText text NOT NULL,
	   isRead boolean not null,
	   notificationDate date not null,
     constraint ID_NOTIFICATION_ID primary key (notificationID));

-- Constraints Section
-- ___________________

alter table CAR add constraint ID_CAR_CHK
     check(exists(select * from CAR_SALE_INFO
                  where CAR_SALE_INFO.carID = carID));

alter table CAR add constraint FKorder_car_FK
     foreign key (orderID)
     references ORDER_DETAILS;

alter table CAR add constraint FKbelongs_to_FK
     foreign key (brandID)
     references BRAND;

alter table CAR_SALE_INFO add constraint FKcar_details_FK
     foreign key (carID)
     references CAR;

alter table ORDER_DETAILS add constraint ID_ORDER_DETAILS_CHK
     check(exists(select * from CAR
                  where CAR.orderID = orderID));

alter table ORDER_DETAILS add constraint FKpurchase_FK
     foreign key (e_mail)
     references USER;


-- Index Section
-- _____________

create unique index ID_BRAND_IND
     on BRAND (brandID);

create unique index ID_CAR_IND
     on CAR (carID);

create index FKorder_car_IND
     on CAR (orderID);

create index FKbelongs_to_IND
     on CAR (brandID);

create unique index FKcar_details_IND
     on CAR_SALE_INFO (carID);

create unique index ID_ORDER_DETAILS_IND
     on ORDER_DETAILS (orderID);

create index FKpurchase_IND
     on ORDER_DETAILS (e_mail);

create unique index ID_USER_IND
     on USER (e_mail);
